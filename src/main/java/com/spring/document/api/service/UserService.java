package com.spring.document.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.document.api.model.User;
import com.spring.document.api.repository.UserRepository;

@Service
public class UserService {
  
  @Autowired
  private UserRepository userRepository;
  
  public void addUser(User user) {
    userRepository.save(user);
  }

}

package com.spring.document.api.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.document.api.model.User;

public interface UserRepository extends MongoRepository<User, String> {

}

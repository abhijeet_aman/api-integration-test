package com.spring.document.api.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

@Data
@Document(collection="UserData")
public class User {
  
  @Id
  private String name;
  
  private String address;
  
  private long mobile;

}

package com.spring.document.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.document.api.model.User;
import com.spring.document.api.service.UserService;

@CrossOrigin("http://localhost:3001")
@RestController
@RequestMapping("/user")

public class UserController {
  
  @Autowired
  private UserService userService;
  
  @PostMapping()
  public void addUser(@RequestBody User user) {
    userService.addUser(user);
  }

}
